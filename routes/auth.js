import Users from '../models/users';
import express from 'express';
import bcrypt from 'bcrypt';
import crypto from 'crypto';
import config from 'config';
import mongoose from 'mongoose';
import nodemailer from 'nodemailer';
//disable CORS in production
import {check,validationResult} from 'express-validator';
const router = express.Router();
let signInValidation = [
    check('email').isEmail().notEmpty().withMessage({message:"Email Should not be empty"}),
    check('password').notEmpty()
] 
router.post('/',signInValidation,async(req,res)=>{
const errors = validationResult(req);
if(!errors.isEmpty())
{
    res.status(400).send({message:errors.array()});
}
else
{
  let user = await Users.findOne({email:req.body.email});
  if(user===null) return res.status(400).send("Invalid Email or Password");
  const isValidPass = await bcrypt.compare(req.body.password,user.password);
  if(!isValidPass) res.status(400).send("Invalid Email or Password");
  if(!user.isVerified)
  {
    if(!user.token)
    {
    const session = await mongoose.startSession();
    session.startTransaction();
    let token = await Users.findByIdAndUpdate(user._id,{token:crypto.randomBytes(16).toString('hex')});
    if(!token)
    {
      session.abortTransaction().then(()=>{session.endSession()})
      res.status(500).send("Internal Server Error");
    }
    else
    {  
      let transporter = nodemailer.createTransport({host:config.get('mailServer'),secure:true,auth:{user:config.get('mail'),pass:config.get("mailPassword")}});
      let mailOptions = {from:config.get('mailServer'),to:user.email,subject:"SkillJedi Account Verification",text:'Hello,\n\n' + 'Please verify your account by clicking the link: \nhttp:\/\/' +"localhost:3000"+ '\/verify\/' + user.token + '.\n'}
      transporter.sendMail(mailOptions).then(()=>{
        session.commitTransaction().then(()=>{session.endSession();});
        res.status(200).send('Old token expired another verification email has been sent to you')
          })
      .catch((err)=>{
        session.abortTransaction().then(()=>{session.endSession()})
        res.status(500).send("Internal Server Error")});
    }
    }
    else
    {
      res.status(401).send("Please Verify Your Email Before Sign in");
    }     
  }
  else
  {
    const token = user.generateAuthToken();
    res.status(200).send({token:token});
  }   
}
});

export default router;