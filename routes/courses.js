import express from 'express';
import {body,validationResult} from 'express-validator';
import authorization from '../middlewares/authorization';
import cors from 'cors';
import checkInstructor from '../middlewares/checkinstructor';
const devDebug = require('debug')('dev:debug');
import Course from '../models/course';
const router = express.Router();
//TODO Partial Delete
//TODO Retrieve Course based on profile preference and userpreference
//TODO Partial Update sanitize function modify to remove empty array
//TODO add Validation for update and create operations for description
router.get('/',cors(),async (req,res)=>{
    const course_data = await Course.find().select({_id:1,name:1,category:1,logo:1,description:1});
    res.status(200).send(course_data);
});
router.get('/:id',async(req,res)=>{
  Course.findById(req.params.id,function(err,course){
   if(!err)
   res.send(course);
   else
   res.status(404).send({msg:"Course Not Found"});
  });
});
let update_validation = [
  body('name').notEmpty().optional({nullable:true,checkFalsy:true}).exists({checkNull:true}).withMessage("Course Name Should Not Be Empty"),
  body('section_name').notEmpty().optional().isArray().exists({checkNull:true}).withMessage("Section Should Have a Name"),
  body('section_details.*.title').notEmpty().optional({nullable:true}).exists({checkNull:true}).withMessage("Course Content Should Have a Title"),
  body('section_details').isArray().notEmpty().optional({nullable:true}).exists({checkNull:true}).withMessage("Sections cannot be Empty"),
  body('section_details.*.url').notEmpty().optional({nullable:true}).exists({checkNull:true}).withMessage("Video's or Document's Url Should be uploaded"),
  body('section_details.*.isVideo').notEmpty().optional({nullable:true}).exists({checkNull:true}).withMessage("Is it a Video or Project"),
  body('description').notEmpty().optional({nullable:true}).exists({checkNull:true}).withMessage("provide a short description")   
 ]
router.patch('/:id',update_validation,[authorization,checkInstructor],async(req,res)=>{
    
    const errors = validationResult(req);
    if(!errors.isEmpty())
    {
      res.status(422).send(errors.array())
    }
    else
    {
      devDebug("Course Update endpoint");    
      let updated_course = 
      {
        name:req.body.name,
        '$addToSet':{section_name:{'$each':req.body.section_name},section_details:req.body.section_details}
      }
      devDebug(updated_course); 
      let newObject=cleanObject(updated_course);
      devDebug(newObject);
      Course.findByIdAndUpdate(req.params.id,newObject,function(err,course){
        if(!err)
        {
          res.status(200).send("Course Updated Successfully");
        }
        else
        {
          res.status(400).send({msg:err.message});
        }
      });
    }
});
function cleanObject(obj) {
  for (let key in obj) {
    if (obj[key]===undefined||obj[key]===null) {
      delete obj[key];
    }
  }
  return obj;
}
let create_validation =[
  body('name').notEmpty().withMessage("Course Name Should Not Be Empty").exists(),
  body('section_name').notEmpty().withMessage("Section Should Have a Name").exists().isArray(),
  body('section_details.*.title').notEmpty().withMessage("Video or Project Should Have a Title").exists(),
  body('section_details').isArray().notEmpty().withMessage("Sections cannot be Empty").exists(),
  body('section_details.*.url').notEmpty().withMessage("Video's or Document's Url Should be uploaded").exists(),
  body('section_details.*.isVideo').notEmpty().withMessage("Is it a Video or a Coding Environment").exists(),
  body('description').notEmpty().withMessage("provide a short description").optional()  
]
router.post("/",create_validation,[authorization,checkInstructor],(req,res)=>
{
  const errors = validationResult(req);
  if(!errors.isEmpty())
  {
      res.status(422).send(errors.array());
  }
  else
  {
    devDebug("Course Creation Endpoint");
  
    let course = new Course({
      name:req.body.name,
      logo:"",
      section_name:req.body.section_name,
      section_details:req.body.section_details,
      tags:req.body.tags
    });
    course.save((err,result)=>{
     if(!err)
     {
       res.status(200).send("Courses Created");
     }
     else
     {
       res.status(400).send({msg:err.message});
     } 
    });
  }
});

router.delete('/:id',[authorization,checkInstructor],(req,res)=>{
  devDebug("Course Deletion EndPoint");
  Course.findByIdAndDelete(req.params.id,(err,result)=>{
    if(!err)
    res.status(200).send("Course Deleted Successfully")
    else
    res.status(404).send({msg:"Course Not Found"})
  })
});

/*router.delete('/',(req,res)=>{

});
*/
export default router;