import express from "express";
import { body, validationResult } from "express-validator";
import authorization from "../middlewares/authorization";
const devDebug = require("debug")("dev:debug");
import Profile from "../models/profile";
const router = express.Router();
//TODO Routes /profile create,read,update,delete endpoint
//TODO Routes /profile/dashboard
//TODO Routes /profile/settings

router.get("/",[authorization],async (req, res) => {
  //Display Profile
  const profile = await Profile.findOne({userId: req.body.decodedData._id});
  if (!profile) res.status(404).send("User Profile Not Found");
  else res.status(200).send(profile);
});

let update_skills = [
  body("skills").isArray().notEmpty().optional({ nullable: true }).exists({ checkNull: true }).withMessage("Skills cannot be empty!!")
];
router.patch("/skills/:id",[authorization,update_skills], async (req, res) => {
  //Update Skill Tags
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    res.status(422).send({ errors: errors.array() })
  }
  else {
    devDebug("Skills Update endpoint");
    const query = {
      username: req.params.id
    };
    const new_skills = {
      skills: req.body.skills
    };
    const updated_profile = Profile.findOneAndUpdate(query, new_skills, { new: true }, function (err, profile) {
      if (!err) {
        res.status(200).send(profile);
        devDebug("Skills Updated Successfully");
      } else {
        res.status(400).send({
          errors: err.message
        });
      }
    });
  }
});

let update_experience = [
  body("experience").isArray().notEmpty().optional({ nullable: true }).exists({ checkNull: true }).withMessage("Experience cannot be empty!!")
];
router.patch("/experience", [authorization,update_experience], async (req, res) => {
  //Update Work Experience
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    res.status(422).send({ errors: errors.array() })
  }
  else {
    devDebug("Experience Update endpoint");
    const query = {
      username: req.params.id
    };
    const new_experience = {
      experience: req.body.experience
    };
    const updated_profile = Profile.findOneAndUpdate(query, new_experience, { new: true }, function (err, profile) {
      if (!err) {
        res.status(200).send(profile);
        devDebug("Experience Updated Successfully");
      } else {
        res.status(400).send({
          errors: err.message
        });
      }
    });
  }
});

let update_qualification = [
  body("qualification").isArray().notEmpty().optional({ nullable: true }).exists({ checkNull: true }).withMessage("Qualification cannot be empty!!")
];
router.patch("/qualification/:id", update_qualification, async (req, res) => {
  //Update Educational Qualifications
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    res.status(422).send({ errors: errors.array() })
  }
  else {
    devDebug("Qualification Update endpoint");
    const query = {
      username: req.params.id
    };
    const new_qualification = {
      qualification: req.body.qualification
    };
    const updated_profile = Profile.findOneAndUpdate(query, new_qualification, { new: true }, function (err, profile) {
      if (!err) {
        res.status(200).send(profile);
        devDebug("Qualification Updated Successfully");
      } else {
        res.status(400).send({
          errors: err.message
        });
      }
    });
  }
});

let update_achievements = [
  body("achievements").isArray().notEmpty().optional({ nullable: true }).exists({ checkNull: true }).withMessage("Achievements cannot be empty!!")
];
router.patch("/achievements/:id", async (req, res) => {
  //Update Achievements
  //TODO Check Instructor/Admin Athorization
  //TODO Course/Project Course Record
});
router.get("/dashboard/", async (req, res) => {
  devDebug("Dashboard EndPoint");
  const profile = await Profile.findOne({
    username: req.body.id
  }).select({ achievements: 1 });
  if (!profile) res.status(404).send({
    message: "User Profile Not Found"
  });
  else res.send(profile);
});

router.delete('/:id', async (req, res) => {
  //TODO Add Athorization
  devDebug("Profile Deletion EndPoint");
  Profile.findOneAndDelete({ username: req.params.id }, (err, result) => {
    if (!err)
      res.status(200).send({ message: "Profile Deleted Successfully" })
    else
      res.status(404).send({ errors: "Profile Not Found" })
  })
});

export default router;