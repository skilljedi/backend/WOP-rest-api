import Users from '../models/users';
import express from 'express';
import {check,validationResult} from 'express-validator';
import bcrypt from 'bcrypt';
const devDebug = require('debug')('dev:debug');
import crypto from 'crypto';
import mongoose from 'mongoose';
import nodemailer from 'nodemailer';
import  config  from 'config';
//TODO if verified remove expires field and token field
//TODO User Account to be deleted if not validated in 2 days
const router = express.Router();
let registerValidation = [
    check('username').notEmpty().isString().withMessage("Should not be empty").custom(async(value)=>{
      let doc = await Users.findOne({username:value});
      if(doc) return Promise.reject("Username Should Be Unique");
      else return true;   
    }),
    check('email').isEmail().notEmpty().withMessage("Email Should be Valid and not empty").custom(async(value)=>{
      let doc = await Users.findOne({email:value});
      if(doc) return  Promise.reject("Email is already registered");
      else return true;  
    }),
    check('password').notEmpty().bail().matches(/^(?=.*[A-Za-z])(?=.*\d)(?=.*[@$.!%*#?&])[A-Za-z\d@$.!%*#?&]{8,}$/).withMessage(" minimum eight characters, at least one letter, one number and one special character")
] 
router.post('/register',registerValidation,async(req,res)=>{
const errors = validationResult(req);
if(!errors.isEmpty())
{
    res.status(400).send(errors.array());
}
else
{
  try{
    let reqData = {
        username:req.body.username,
        email:req.body.email,
        password:req.body.password,
    }
    const session = await mongoose.startSession();
    session.startTransaction();
    const salt = await bcrypt.genSalt(10);
    reqData.password  = await bcrypt.hash(reqData.password,salt);
    reqData.token = crypto.randomBytes(16).toString('hex');
    let user = new Users(reqData);
    const userRes =await user.save({session:session})
    if(!userRes)
    {
      res.status(500).send("Internal Server Error");      
    }
    else
    {
      let transporter = nodemailer.createTransport({host:'smtp.gmail.com',secure:true,auth:{user:config.get('mail'),pass:config.get("mailPassword")}});
      let mailOptions = {from:"arpapps843@gmail.com",to:user.email,subject:"SkillJedi Account Verification",text:'Hello,\n\n' + 'Please verify your account by clicking the link: \nhttp:\/\/' +"localhost:3000"+ '\/verify\/' + userRes.token + '.\n'}
      transporter.sendMail(mailOptions).then(()=>{
        session.commitTransaction().then(()=>{session.endSession()});
        res.status(200).send('A verification email has been sent to ' + user.email + '.')})
        .catch((err)=>{
          session.abortTransaction().then(()=>{session.endSession()}); 
          res.status(500).send(err.message)});       
    }
    
  }
  catch(err)
  {
    devDebug(err.message)
    res.status(500).send("Internal Server Error");
  }
  }
});
router.post('/verify',async(req,res)=>{
 try
 {
  let user = await Users.findOne({token:req.body.token});
  if(!user)res.status(404).send("User not verified,Token may have expired");
  if(user.isVerified)res.status(200).send("User Already Verified");
  user.isVerified = true;
  user.token = undefined;
  user.createdAt = undefined;
  let userFinalSave = await user.save();
  if(userFinalSave) return res.status(200).send("User Verified Successfully You can Login");
  else  return res.status(200).send("User Verified Successfully You can Login");
  }
  catch(err)
  {
    devDebug(err)
    res.status(500).send("Internal Server Error");
  }   
  });
export default router;