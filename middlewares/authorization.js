import jwt from 'jsonwebtoken';
import config from 'config';
function authorization(req,res,next)
{
    const token = req.header("x-auth-token")
    if(!token) return res.status(401).send("Unauthorized")
    try {
        const decodedData = jwt.verify(token,config.get('jwtPrivateKey'));
        req.body = decodedData
        next();
        
    } catch (error) {

        res.status(400).send("Invalid Token");
    }
}

export default authorization;