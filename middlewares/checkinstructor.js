function checkInstructor(req,res,next)
{
if(!req.body.isInstructor) res.status(403).send("Forbidden");
next();
}

export default checkInstructor;