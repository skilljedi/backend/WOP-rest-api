import mongoose from 'mongoose';
import jwt from 'jsonwebtoken';
import config from 'config';
const userSchema = new mongoose.Schema({
    username:{
        type:String,
        required:[true,'username is mandatory'],
        unique:[true,'Username already taken'],
        minlength:[5,"Username should have minimum length of five"],
        maxlength:20
    },
    email:{
        type:String,
        unique:true,
        maxlength:255,
        unique:true,
        required:[true,'Email Should not be empty']
    },
    password:{
        type:String,
        minlength:5,
        maxlength:1024,
        required:[true,'password is mandatory']
    },
    isInstructor:{
        type:Boolean,
        default:false
    },
    token:{
        type:String,
    },
    createdAt:{
        type:Date,
        default:Date.now,
        expires:43200
    },
    isVerified:{
        type:Boolean,
        default:false
    }
});

userSchema.methods.generateAuthToken = function(){
    const token = jwt.sign({_id:this._id,isInstructor:this.isInstructor},config.get('jwtPrivateKey'));
    return token;
}

export default mongoose.model('user',userSchema);