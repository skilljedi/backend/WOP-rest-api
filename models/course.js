import mongoose from 'mongoose';
const courseSchema = new mongoose.Schema({
    name:{
        type:String,
        required:[true,'course name cannot be null']},
        uploaded_date:{
            type:Date,
            default:Date.now
        },
        description:{
            type:String,
            default:"Course with Mini Project",
            required:true
        },
        progress:{
            type:Number,
            min:0,
            max:100
        },   
    logo:String,
    section_name:[
        {
        type:String,
        required:[true,'section name is mandatory']
        }
        ],
    section_details:[{
        title:{type:String,required:[true,"Give video title"]},
        url:{type:String,required:[true,"Provide link of video"]},
        isVideo:{type:Boolean,required:[true,"Is it a video or a project"]}}],
    tags:[String]
 });


 courseSchema.methods.searchByTag = function(tag){
     return mongoose.model('Course').find({tags:tags.map((tgname)=>tgname===tag)})
 }

 courseSchema.methods.createCourse = function(name,logo,section_name)
 {
     return mongoose.model('Course').save({name:name,logo:logo,section_name:section_name});
 }
export default mongoose.model('Course',courseSchema);