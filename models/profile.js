import mongoose from "mongoose";

const profileSchema = new mongoose.Schema({
  username: {
    type: String,
    required: [true, "username is mandatory"],
  },
  fullname: {
    type: String,
    maxlength: 255,
    required: [true, "Name cannot be empty"],
  },
  avatar: {
    type: String,
  },
  skills: [{ type: String }],
  achievements: [
    {
      name: String,
      description: String,
      logo: String,
      progress: String,
      isProject: Boolean,
    },
  ],
  experience: [
    {
      company: String,
      designation: String,
      years: String,
    },
  ],
  qualification: [
    {
      institute: String,
      stream: String,
      year: String,
      grade: String,
    },
  ],
});

export default mongoose.model("profile", profileSchema);
