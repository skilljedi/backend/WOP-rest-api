import express from 'express';
const basicDebuger = require('debug')('basic:debug');
import mongoose from 'mongoose';
import config from 'config';
import auth from './routes/auth';
import cors from 'cors';
import courses from './routes/courses';
import users from './routes/users';
import home from './routes/home';
import profile from "./routes/profile";

//TODO remove app.use(cors()) in production
const app =express();
if(!config.get('jwtPrivateKey'))
{
    console.error("FATAL ERROR EXITING!!!!!!!!!!!");
    process.exit(1);
}
if (!config.get("database")) {
  console.error("SET DATABASE!!!!!!");
  process.exit(1);
}
const port = process.env.PORT ? process.env.PORT : 3001;
mongoose.connect(config.get("database"), {
  useNewUrlParser: true,
  useUnifiedTopology: true,
  useFindAndModify: false,
});
mongoose.connection.once("open", () => {
  basicDebuger("Connected To MongoDB");
});
mongoose.connection.once("error", (err) => {
  basicDebuger(err.message);
});
app.use(express.json());
app.disable('x-powered-by');
//serverDebugger("Type anything related to app to debug")
//export DEBUG=server:debugger
app.use(cors());
app.use('/',home);    
app.use('/courses',courses);
app.use('/users',users);
app.use('/auth',auth);
app.use("/profile", profile);

app.listen(port, () => basicDebuger(`Server Started at ${port}`));
