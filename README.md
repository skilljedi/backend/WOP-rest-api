# WOP-rest-api
[![time tracker](https://wakatime.com/badge/github/adityarpai843/WOP-rest-api.svg)](https://wakatime.com/badge/github/adityarpai843/WOP-rest-api)
<h3>How to run? and contribute</h3>
<ul>
<li>Clone this repository </li>  
<li>Create a branch with your name on this repo <b>FORKS ARE PROHIBITED</b></li>
<li>Go to working directory and npm install</li>
<li>Ensure That mongodb is installed in your system</li>
<li>Set these environment variables</li>
<ol>
<li>WOP_EMAIL</li>
<li>WOP_DATABASE(Database connection String)</li>
<li>WOP_MAIL_PASSWORD</li>
<li>WOP_JWT_KEY=HELLOWORLDWOP (For the time being use this JWT key)</li>
</ol>     
<li>create a database with name <b>wop</b></li> 
<li>npm run dev</li>  
</ul>  
<h3>Conventions for Error response objects</h3>
<ul>
<li>if the response is result of user-validation using express validator then
<i>res.status(422).send(error.array())</i></li>
<li>else it should be<i>res.status(500).send("Internal Server Error")</i>
</li>
<li>for displaying error objects use dev:debug by 
<i>
const devDebug = require('debug')('dev:debug');
devDebug(error)
will display error object on console where you are running 
</i>
</li>   
